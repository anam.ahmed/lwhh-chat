var socket   = io();
var nickname = prompt("Enter a nickname");
var uuid     = Date.now().toString()+Math.round(Math.random()*100).toString();

console.log(uuid); 
socket.on('message',function(message){
    console.log(message);
    var messageEl = document.createElement('div');
    var messageInner = "<span class='actor'>"+message.nickname+":</span> "+message.text;
    messageEl.innerHTML = messageInner;
    var classes ="message";
    if(message.uuid===uuid){
        classes = classes + " me";
    }
    messageEl.className = classes;
    document.querySelector('.messages').appendChild(messageEl);
});

document.querySelector('.text').addEventListener('keyup',function(e){
    if(e.keyCode!==13) return;
    if(!e.target.value.trim()) return;
    console.log(e.target.value);
    socket.emit('message',{
        text:e.target.value,
        uuid:uuid,
        nickname:nickname
    });
    e.target.value="";
})