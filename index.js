const express  = require('express');
const socketio = require('socket.io');
const http     = require('http');

const app      = express();
const httph    = http.Server(app);
const io       = socketio(httph);

app.use(express.static('./public'));

io.on('connection',socket=>{
    console.log("A client connected");
    socket.on('message',message=>{
        io.emit('message',message);
    });
    io.emit('message',{
        text:"A new person joined",
        nickname:"system",
        uuid:"1"
    })
});

httph.listen(process.env.PORT || 4000,()=>{
    console.log(`Application listening on ${process.env.port || 4000}`);
});